#!/usr/bin/perl
#
# IMPORTANT SECURITY NOTE.
#
# Configuration files should be rwx-only for root
#
#      - there is a mysql password
#      - this file is executed
#
# While it offers the incredible advantage to avoid reinventing a boring
# config parser and to permit hackers to create very efficient config
# by using perl, if anybody can edit it, anybody can push the software to 
# execute malicious code when looking for it's configuration.
# (But it's like a /etc/profile ... /etc is for admins).
#
# By default:
#      Access: (0750/-rwxr-x---)
#      Uid: (    0/root)
#      Gid: (    0/root)

use strict;

## GENERAL
# Delay to keep IP in the database
our $delay="3600";


## SQL DATABASE
our $db_host="localhost";
our $db_name="seeyoulater";
our $db_user="root";
our $db_password="tralala";

## UNAUTHENTICATED IMPORT/EXPORT
# If you have several hosts on the same network, you may want to use the 
# same database to share all the bans.
# If you want to share bans outside a secure network, seeyoulater can produce
# an extra export file that can be loaded over http on any other seeyoulater
# instance.

# Export file path, like /var/www/seeyoulaterlist or whatever
##our $share_exportfile = "/var/www/file";
# Import urls, like http://distanthost/seeyoulaterlist
##our @share_importurls = ("http://url/file");


## LOGS TO STUDY
# Mail server logfile 
our $logfile = "/var/log/exim4/rejectlog";

# EOF

