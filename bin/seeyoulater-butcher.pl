#!/usr/bin/perl
#
# Copyright (c) 2006-2015 Mathieu Roy <yeupou--gnu.org>
# http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;
use Fcntl qw(:flock);
use Term::ANSIColor qw(:constants);
use DBI;
use POSIX qw(strftime);
use File::Copy;
use File::Temp qw(tempfile tempdir);
use Time::Local;


###############################################
###############################################
# Initialize

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

my $conffile = "/etc/seeyoulater.conf.pl";
my $denyfile = "/etc/hosts.deny";
my ($getopt, $flush, $help);

our $db_host;
our $db_name;
our $db_user;
our $db_password;

# Get options
eval {
    $getopt = GetOptions("help" => \$help,
			 "flush" => \$flush);
};

# Def return
if ($help) {
    print STDERR <<EOF;
Usage: $0 [OPTION]
Update $denyfile according to the banned IPs database as configured 
in $conffile

    -h, --help                 display this help and exit

Report bugs and suggestions at <https://gna.org/projects/seeyoulater/>
or to <yeupou -- gnu.org>
EOF
exit(1);
}


# Get conffile
do $conffile or die "Unable to run $conffile.\n", RED,"Most commonly, it's a privilege issue.",RESET,"\n\nStopped";

# Connect to database
my $dbd =DBI->connect('DBI:mysql:database='.$db_name.':host='.$db_host,
		      $db_user, 
		      $db_password,
		      { RaiseError => 1, AutoCommit => 1});


###############################################
###############################################
# Build temporary hosts.deny

# Create the temporary file
my ($tempfilefh, $tempfile) = tempfile(UNLINK => 1);

# Get entries not managed by seeyoulater from the current hosts.deny
open(DENYFILE, "< $denyfile");
while(<DENYFILE>) {
    next if(/^\#\# seeyoulater-butcher begin/ .. /^\#\# seeyoulater-butcher end/);
    print $tempfilefh $_;
}
close(DENYFILE); 

# Now print the current database entries
# There may be none, it does not matter, we wont check.
# FIXME: Should we limit the filesize? Is there any limit to hosts.deny?

# This will not work properly if this is a not on a newline
print $tempfilefh "## seeyoulater-butcher begin\n";
print $tempfilefh "# ".timelocal(localtime())."\n";

my $hop;
# Extract distinct IPs: the same IP may be registered several times in the
# database, we will anyway add it only once 
$hop = $dbd->prepare("SELECT DISTINCT ip_address FROM ips");
$hop->execute;
while (my ($ip) = $hop->fetchrow_array) {
    print $tempfilefh "ALL: $ip\n";    
}
$hop->finish;

print $tempfilefh "## seeyoulater-butcher end\n";
close($tempfilefh);  

# For now, overwrite without further test. Maybe it would be cleaner to
# only update when different
system("chmod", "a+r", $tempfile);
move($tempfile, $denyfile);


# EOF
