#!/usr/bin/perl
#
# Copyright (c) 2006-2015 Mathieu Roy <yeupou--gnu.org>
# http://yeupou.wordpress.com
#
# Partly based on Vincent Caron's logalert
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;
use Fcntl qw(:flock);
use Term::ANSIColor qw(:constants);
use DBI;
use POSIX qw(strftime);
use File::Temp qw(tempfile tempdir);
use Time::Local;

###############################################
###############################################
# Initialize

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

my $conffile = "/etc/seeyoulater.conf.pl";
my $temp  = tempdir(CLEANUP => 1);
my $cache = "/var/cache/seeyoulater";
my $changesfile   = "$temp/changed";
my ($getopt, $flush, $help);

our $db_host;
our $db_name;
our $db_user;
our $db_password;
our $delay = "3600";
our $logfile = "/var/log/exim4/rejectlog";

# Get options
eval {
    $getopt = GetOptions("help" => \$help,
			 "flush" => \$flush);
};

# Def return
if ($help) {
    print STDERR <<EOF;
Usage: $0 [OPTION]
Feeds the banned IPs database as configured in $conffile

    -h, --help                 display this help and exit
    -f, --flush                flush the database before registering
                               new IPs.

Report bugs and suggestions at <https://gna.org/projects/seeyoulater/>
or to <yeupou -- gnu.org>
EOF
exit(1);
}

# Get conffile
do $conffile or die "Unable to run $conffile.\n", RED,"Most commonly, it's a privilege issue.",RESET,"\n\nStopped";

# Connect to database
my $dbd =DBI->connect('DBI:mysql:database='.$db_name.':host='.$db_host,
		      $db_user, 
		      $db_password,
		      { RaiseError => 1, AutoCommit => 1});


###############################################
###############################################
# subs

# logtail sub grabbed from logalert
# This is totally inspired from logtail/logcheck by Middleton & Slootman
sub logtail {
    my $log = shift;
    my $tail = shift;
    my $status = "$cache".($log =~ m:^/: ? '' : '/')."$log.seeyoulater";

    sub mkdir_p {
        my $path = shift;
        $path =~ s:[^/]*$::; # Remove file name
	return system('mkdir', '-p', $path);
    }

    if (not open(LOG, "<$log")) {
        return 0;
    }
    if (not open(TAIL, ">$tail")) {
        return 0;
    }

    my $inode  = 0;
    my $offset = 0;
    if (open(STATUS, "<$status")) {
        while (<STATUS>) {
            chomp;
            my ($key, $val) = split / /;
            $inode = $val if $key eq 'inode';
            $offset = $val if $key eq 'offset';
        }
        close STATUS;
    }
    
    my (undef, $log_inode, undef, undef, undef, undef, undef, $log_size) = stat($log);
    if ($inode == $log_inode) {
        if ($offset > $log_size) {
            # file size is smaller than the last time it was checked,
	    #it could indicate tampering.
            $offset = 0; # safer choice, let's start from the beginning
        }
    } else {
        # inode changed, reset offset to zero (mostly due to logrotate)
        $offset = 0;
    }

    # Copy tail line by line (not efficient, but makes sure the tail
    # is ending with a newline)
    seek(LOG, $offset, 0);
    print TAIL while (<LOG>);
    $offset = tell LOG;

    close(TAIL);
    close(LOG);

    umask 0027;
    if (mkdir_p($status) or not open(STATUS, ">$status")) {
        # could not write status, the same information will be
	# parsed again next time 
        return 1;
    }
    print STATUS "inode $log_inode\noffset $offset\n";
    close STATUS;
    return 1;
}

###############################################
###############################################
# Keep database clean

# Remove too old entries
my $limit_flush = "WHERE time<".(timelocal(localtime()) - $delay);
# Or all if flush was asked
$limit_flush = "" if $flush;

$dbd->do("DELETE FROM ips ".$limit_flush);


###############################################
###############################################
# Grab data and feed the database

# Take apart the unknown bits of log
logtail($logfile, $changesfile);

# Notify bans requested
# We only look for strings like ++BAN:nnn.nnn.nnn.nnn++
open(IN, "< $changesfile");
my @ips;
my %seen_before;
while (<IN>) {
    my $ip;

    # Ignore lines without even the begin of the string
    next unless /\+\+BAN\:/;

    # If found, find out the IP (for now, IPv4 only, but that could be
    # easily improved)
    $ip = $1 if /\+\+BAN\:([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\+\+/;
    
    # If no ip still found despite the ++BAN string was found, ignore
    next unless $ip;

    # We do not want to run a SQL insert for each ip found, but we do not
    # to store too many IPs in a @list. So if we will proceed per 50 ips batch
    push(@ips, $ip)
	unless $seen_before{$ip};
	
    # Remember we already caught that IP this run
    # (this should scale; it would require a heavy ddos to have many differents
    # ips)
    $seen_before{$ip} = 1;
    
    # According to what has been said, do the mysql insert only if bigger than
    # 50.
    # Lock the table for max efficiency
    next unless (scalar(@ips) > 49);

    # Prepare the sql insert
    my $time = timelocal(localtime());
    my $sql = "INSERT INTO ips VALUES ";
    for (@ips) {
	$sql .= "('$_','$time'),";
    }
    chop($sql);

    # Run it
    $dbd->do($sql);

    # Purge the ip list
    $#ips = 0;
}
close(IN);

# If the number of IP was below 50, no insert what made before

if (scalar(@ips) > 0) {
    # Prepare the sql insert
    my $time = timelocal(localtime());
    my $sql = "INSERT INTO ips VALUES ";
    for (@ips) {
	$sql .= "('$_','$time'),";
    }
    chop($sql);
    
    # Run it
    $dbd->do($sql);

}


# EOF
