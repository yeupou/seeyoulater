#!/usr/bin/perl
#
# Copyright (c) 2014 Mathieu Roy <yeupou--gnu.org>
# http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;
use Fcntl qw(:flock);
use Term::ANSIColor qw(:constants);
use LWP::UserAgent;
use HTTP::Request;
use DBI;
use POSIX qw(strftime);
use File::Copy;
use File::Temp qw(tempfile tempdir);
use Time::Local;
use URI::Escape;

###############################################
###############################################
# Initialize

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

my $conffile = "/etc/seeyoulater.conf.pl";
my $temp  = tempdir(CLEANUP => 1);
my ($getopt, $help);

our $db_host;
our $db_name;
our $db_user;
our $db_password;
our $share_exportfile;
our @share_importurls;


# Get options
eval {
    $getopt = GetOptions("help" => \$help,);
};

# Def return
if ($help) {
    print STDERR <<EOF;
Usage: $0 [OPTION]
Import \@share_importurls and export to \$share_exportfile as configured
in $conffile.

    -h, --help                 display this help and exit

Report bugs and suggestions at <https://gna.org/projects/seeyoulater/>
or to <yeupou -- gnu.org>
EOF
exit(1);
}

# Get conffile
do $conffile or die "Unable to run $conffile.\n", RED,"Most commonly, it's a privilege issue.",RESET,"\n\nStopped";

# Direct exit if no import and no export are required.
exit unless scalar(@share_importurls) or $share_exportfile;

# Connect to database
my $dbd =DBI->connect('DBI:mysql:database='.$db_name.':host='.$db_host,
		      $db_user, 
		      $db_password,
		      { RaiseError => 1, AutoCommit => 1});


###############################################
###############################################
# EXPORT / SLURP DATABASE

# We want to be able to rely on the export mtime afterwards, so we cannot
# simply overwrite the export file each run

# First identify what is already exported
my %seen_before;
my %orphan;
if (-e $share_exportfile) {
    # If it already exists, first get a list of ip it contains
    open(CURRENT, "< $share_exportfile");
    while (<CURRENT>) { 
	# IPv4
	if (/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$/) {
	    $seen_before{$1} = 1;
	    $orphan{$1} = 1;
	}
    }
    close(CURRENT);
}


# Then identify what is currently in the database. On the fly update the 
# export file if any new IP is found

# (Write only if requested in conf)
open(CURRENT, ">> $share_exportfile") if $share_exportfile;

# Extract distinct IPs: the same IP may be registered several times in the
# database, we will anyway add it only once 
my $hop;
$hop = $dbd->prepare("SELECT DISTINCT ip_address FROM ips");
$hop->execute;
while (my ($ip) = $hop->fetchrow_array) {
    # is not an orphan
    delete($orphan{$ip});
    # ignore if already known
    next if $seen_before{$ip};
    # otherwise add to the list, if requested
    print CURRENT "$ip\n" if $share_exportfile;
    # and remember for later
    $seen_before{$ip} = 1;
}
$hop->finish;
close(CURRENT) if $share_exportfile;


# Finally, find out if by any chance our export file does not contains
# orphaned (no longer in database) IPs.
# If there is an orphaned IP, it's straighforward to rewrite the 
# export file no matter what
if ((scalar(keys %orphan) > 0) and $share_exportfile) {
    open(CURRENT, "> $share_exportfile");
    foreach (sort keys %seen_before) {
	# skip if orphaned
	next if $orphan{$_};
	print CURRENT "$_\n";
    }
    close(CURRENT);
}


###############################################
###############################################
# IMPORT

if (scalar(@share_importurls)) {
    # We want to cache each url so we can update them only if necessary
    # based on mtim
    foreach my $url (@share_importurls) {
        # SSL verifies none = ugly but handle selfies certs
	my $useragent = LWP::UserAgent->new(
	    agent => "SeeYouLater",
	    timeout => 10,
	    ssl_opts => { SSL_verify_mode => 'SSL_VERIFY_NONE', verify_hostname => 0}, 
	    );
	# cache the file in /tmp
	my $cachefile = "/tmp/seeyoulater".uri_escape($url);
	my $result = $useragent->mirror($url, $cachefile);

	# skip in case of failure
	next if $result->is_error();

	# add the unseen_before ip to the database like the feeder does
	open(IN, "< $cachefile");
	my @ips;
	while (<IN>) {
	    my $ip;

	    # look for IPv4
	    $ip = $1 if /^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$/;
	    
	    # break out this file if anything else found
	    last unless $ip;
	    
	    # We do not want to run a SQL insert for each ip found, 
	    # but we do not
	    # to store too many IPs in a @list. 
	    # So if we will proceed per 50 ips batch
	    push(@ips, $ip)
		unless $seen_before{$ip};
	
	    # Remember we already caught that IP this run
	    # (this should scale; it would require a heavy ddos
	    # to have many differents ips)
	    $seen_before{$ip} = 1;
    
	    # According to what has been said, 
	    # do the mysql insert only if bigger than 50.
	    # Lock the table for max efficiency
	    next unless (scalar(@ips) > 49);

	    # Prepare the sql insert
	    my $time = timelocal(localtime());
	    my $sql = "INSERT INTO ips VALUES ";
	    for (@ips) {
		$sql .= "('$_','$time'),";
	    }
	    chop($sql);
	    
	    # Run it
	    $dbd->do($sql);
	    
	    # Purge the ip list
	    $#ips = 0;
	}
	close(IN);
	
        # If the number of IP was below 50, no insert what made before
	if (scalar(@ips) > 0) {
	    # Prepare the sql insert
	    my $time = timelocal(localtime());
	    my $sql = "INSERT INTO ips VALUES ";
	    for (@ips) {
		$sql .= "('$_','$time'),";
	    }
	    chop($sql);
	    
	    # Run it
	    $dbd->do($sql);	    
	}
    }    
}

# EOF
