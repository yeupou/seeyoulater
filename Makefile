
all:
	@echo bouh!

deb:
# remove previous package
	rm -f ../seeyoulater_* ../seeyoulater-*
	dpkg-buildpackage -rfakeroot -uc -us
# remove build dir
	rm -rf `find debian -type 'd' -name 'seeyoulater*' -maxdepth 1`
# remove .changes, .tar.gz
	rm -rf ../seeyoulater_*.dsc ../seeyoulater_*.changes ../seeyoulater_*.tar.gz

deb-common:
	mkdir -p $(etc) $(prefix)/share/doc/seeyoulater-common
	install --mode=750 etc/seeyoulater.conf.pl $(etc)/
	install db/create-database.sql $(prefix)/share/doc/seeyoulater-common/
	install README $(prefix)/share/doc/seeyoulater-common/

deb-butcher:
	mkdir -p $(etc)/cron.d $(prefix)/bin
	install --mode=644 etc/cron-butcher $(etc)/cron.d/seeyoulater-butcher
	install bin/seeyoulater-butcher.pl $(prefix)/bin/seeyoulater-butcher

deb-feeder:
	mkdir -p $(etc)/cron.d $(prefix)/bin
	install --mode=644 etc/cron-feeder $(etc)/cron.d/seeyoulater-feeder
	install bin/seeyoulater-feeder.pl $(prefix)/bin/seeyoulater-feeder

deb-httpsharer:
	mkdir -p $(etc)/cron.d $(prefix)/bin
	install --mode=644 etc/cron-httpsharer $(etc)/cron.d/seeyoulater-httpsharer
	install bin/seeyoulater-httpsharer.pl $(prefix)/bin/seeyoulater-httpsharer
